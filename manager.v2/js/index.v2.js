$(document).ready(function () {
    var currentPath, moveAndCopyPath;

    //init call AJAX
    getContents();
    function getContents(path = null, partial = false, location = null) {
        $.ajax({
            url: "php.v2/get_dir.php",
            dataType: "json",
            method: "POST",
            data: { path: path },
            success: function (res) {
                if (res.status == true) {
                    renderWorkSpace(res.data, partial, location)
                } else {
                    bootbox.alert({message: "Error while getting data."})
                }
            }
        });
    };

    //Rander both side workspace
    function renderWorkSpace(data, partial, location) {
        currentPath = data.currentPath
        let allPathSplitArray = currentPath.split("/"); 
        let hierarchyPath = allPathSplitArray.map(function (item, index) {
            let currentFilePath = item;
            let newIndex = index;
            while (newIndex != 0) {
                currentFilePath = allPathSplitArray[newIndex - 1] + "/" + currentFilePath
                newIndex--;
            }
            return currentFilePath;
        }).filter(Boolean)
        hierarchyPath.forEach(function (value, index) { //get hierarchy of folder and file
            $(".menu__box").find('.leftTree[data-val="' + value + '"]').addClass("active")
            $(".menu__box").find('.leftTree[data-val="' + value + '"]').find("img").eq(0).attr("src", "./assets/images/openfolder.png")
        })
        if (data.folders.length >= 0) {
            if (location == "right") {  ///oparend location logic while right side operate
                $(".folder__area .folderTemplate").siblings().remove();
            }
            if (location == "left" && partial == false) { //oparend location logic while left side operate
                $(".menu__box .folderTemplate").siblings().remove();
            }
            if (location == "back" || location == 'delete') { //oparend location logic while delete and back btn press
                $(".folder__area .folderTemplate").siblings().remove();
                if (currentPath) {
                    $(".menu__box").find('.leftTree[data-val="' + currentPath + '"] .childs').eq(0).html("")
                } else {
                    $(".menu__box .childs").eq(1).html("")
                }
            }
            data.folders.forEach((element, i) => {
                renderItem(element, currentPath)
            });
        }

        if (data.files.length > 0) {
            data.files.forEach((element, i) => {
                renderItem(element, currentPath)
            });
        }
    }

    //folder create function
    $(document).on("click", ".create__btn", function () {
        prompt(ajaxCreatCall,'folder'); //bootbox promt call
    });

    //bootbox prompt
    function prompt(callback, methodName) {
        var regx
        methodName == "folder" ? regx = new RegExp("^[a-zA-Z0-9]*$") : regx = new RegExp("^.*\.(jpg|JPG|PNG|png|jpeg|JPEG|xls|XLS|txt|TXT|csv|CSV|zip|ZIP)$");
        bootbox.prompt({
            size: "small",
            title: "Enter File/folder name",
            callback: function (result) {
                if (result != null) {
                    if (regx.test(result)) {
                        callback(result, methodName);
                    }else {
                        bootbox.alert({message: "Enter Proper Name."})
                    }
                }
            }
        });
    }

    //Home btn function
    $(document).on("click", ".home__btn", function () {
        if (currentPath != '') {
            $(".childs").children().remove()
            $(".file").siblings().remove()
            getContents(); //init call            
        }
    })

    //AJAX for creating file or folder
    function ajaxCreatCall(name,type) {
        $.ajax({
            url: "php.v2/creat_dir.php",
            dataType: "json",
            method: "POST",
            data: { fileName: name, path: currentPath, fileType: type },
            success: function (res) {
                if (res.status == true) {
                    renderWorkSpace(res.data, true)
                } else {
                    bootbox.alert({message: "Error while creating file/folder."})
                }
            }
        });
    }

    //single click left side oparation 
    $(document).on("click", ".leftTree", function (event) {
        event.stopPropagation()
        currentPath = $(this).attr("data-val")
        var ext = $(this).find("span").html()
        if (!ext.includes(".")) {
            if ($(this).hasClass("active")) {  //if active class then remove and add folder icon
                $(this).removeClass("active")
                $(this).find("img").attr("src", "./assets/images/folder.png")
                $(this).find(".childs").html("")
            } else {  //if active is not present then add class then replace folder icon to openfolder icon
                $(".folder__area .folderTemplate").siblings().remove();
                $(this).addClass("active")
                $(this).find("img").attr("src", "./assets/images/openfolder.png")
                getContents(currentPath, false, "right")
            }
        }
    });

    //Select folder on click
    $(document).on("click", ".folderClick", function (event) {
        event.ctrlKey ? $(this).addClass("folder__select") : $(this).addClass("folder__select").siblings().removeClass("folder__select")
    });

    //double click opartion for folder open
    $(document).on("dblclick", ".folder__area > div", function () {
        var ext = $(this).find("h2").html()
        if (!ext.includes(".")) {
            currentPath = $(this).parent().attr("data-val");
            currentPath = $(this).attr("data-val");
            getContents(currentPath, true, "right")
        }
    });

    //back btn function
    $(document).on("click", ".back__btn", function () {
        if (currentPath != "") {
            var path = currentPath.substr(0, currentPath.lastIndexOf("/"));
            $(".menu__box .leftTree").removeClass("class");
            getContents(path, true, "back")
        }else{
            bootbox.alert({message : 'You do not have permission to go <b><span class="back__one">BACK</span></b>'})
        }
    });

    //delete btn function
    $(document).on("click", ".delete__btn", function () {
        var select = $(".folder__select");
        var path = [];

        for (let i = 0; i < select.length; i++) { //to collect all select folder and file
            path[i] = select.eq(i).attr("data-val");
        }

        if (select.length > 0) {
            bootbox.confirm({
                size: "small",
                message: "<h2>Are you sure want to delete?<h2>",
                callback: function (result) {
                    if (result == true) {
                        ajaxDelCall(path,currentPath)
                    }
                }
            })
        }

        function ajaxDelCall(path, currentPath) { //delete AJAX call
            $.ajax({
                url: "php.v2/delete_dir.php",
                dataType: "json",
                method: "POST",
                data: { path: path, currentPath : currentPath },
                success: function (res) {
                    if (res.status == true) {
                        getContents(currentPath, true, "delete")                        
                    } else {
                        bootbox.alert({message: "Error while Deleting data."})
                    }
                }
            });
        }
    });

    //add file function
    $(document).on("click", ".add__btn", function () {
        prompt(ajaxCreatCall,'file');
    });

    //move and copy btn
    $(document).on("click", ".move__btn, .copy__btn", function () {
        moveAndCopyPath = moveAndCopy()
        $(this).hasClass("move__btn") ? click = "move" : click = "copy";
    });

    //Paste btn function
    $(document).on("click", ".paste__btn", function () {
        $.ajax({
            url: "php.v2/move_dir.php",
            dataType: "json",
            method: "POST",
            data: { path: moveAndCopyPath, currentPath: currentPath, action: click },
            success: function (res) {
                if (res.data.action == "move" || res.data.action == "copy") {
                    renderWorkSpace(res.data, true)  
                } else {
                    bootbox.alert({message: "Error while Copy/Move data."})
                }
            }
        });
    });

    //render both side iteam [folder and file]
    function renderItem(data, currentPath, location) {
        var leftFolder = $(".menu__box .folderTemplate").clone()
        leftFolder.find("span").html(data['name']);
        leftFolder.find(".leftTree").attr("data-val", data['path']).attr("data-name", data['name']);
        if (currentPath) { //if currentPAth is not room append folder as a sub folder
            leftFolder.find(".leftTree").addClass("m10");
            $(".menu__box .leftTree[data-val='" + currentPath + "'] .childs").eq(0).append(leftFolder.html());
        } else {
            $(".menu__box .childs").eq(1).append(leftFolder.html());
        }
        var rightFolder = $(".folder__area .folderTemplate").clone()
        rightFolder.find("h2").html(data['name']);
        rightFolder.children().attr("data-val", data['path']);
        $(".folder__area").append(rightFolder.html());
        if (data.extension != null) { // data ext is not empty than change file icon as per file
            var dataVal = $("[data-val='" + data.path + "']"); 
            appendExt($(".folder__area, .childs").find(dataVal), data.extension); 
        }
    }

    //multipal folder or file select whil copy and paste
    function moveAndCopy(){
        var select = $(".folder__select");
        var paths = [];
        for (let i = 0; i < select.length; i++) {
            var tempPath = select.eq(i).attr("data-val");
            paths[i] = tempPath;
        }
        return paths;
    }

    //add file and folder
    function appendExt(ele, res) { //add file icon
        $(ele).find("img").attr("src", `./assets/images/${res}.png`)
    }

    //contextmenu
    $.contextMenu({
        selector: '.folderClick',
        callback: function(key, options) {
            $(".context-menu-active").addClass("folder__select");
            switch (key) {
                case ("copy"):
                    $(".copy__btn").trigger("click")
                    break;
                case ("cut"):
                    $(".move__btn").trigger("click")
                    break;
                case ("paste"):
                    $(".paste__btn").trigger("click")
                    break;
                case ("delete"):
                    $(".delete__btn").trigger("click")
                    break;
                case ("rename"):
                   rename();
                    break;
                default:
                    break;
            }
        },

        items: {
            "copy": {name: "Copy", icon: "copy"},
            "cut": {name: "Cut", icon: "cut"},
            "delete": {name: "Delete", icon: "delete"},
            "rename": {name: "Rename", icon: "Rename"},
        }
    });

    //contextmenu
    $.contextMenu({
        selector: '.wrapper:not(.file div)', 
        callback: function(key, options) {
            switch (key) {
                case ("paste"):
                    $(".paste__btn").trigger("click")
                    break;
                case ("Creat Folder"):
                    $(".create__btn").trigger("click")
                    break;
                case ("Creat File"):
                    $(".add__btn").trigger("click")
                    break;
                default:
                    break;
            }
        },
        items: {
            "paste": {name: "Paste", icon: "paste"},
            "Creat Folder": {name: "Creat Folder", icon: "Folder"},
            "Creat File": {name: "Creat File", icon: "File"},
        }
    });

    //rename of file and folder function
    function rename() {      
        
        bootbox.prompt({
            size: "small",
            title: "Rename name...",
            callback: function (result) {
                if (result != '' ) {
                    currentPath =  $(".folder__select").attr("data-val");
                    renameAjax(currentPath, result);
                }else{
                    bootbox.alert({message: "Enter Name."})
                }               
            }
        });

        function renameAjax(currentPath, fname) {
            $.ajax({
                url: "php.v2/rename.php",
                dataType: "json",
                method: "POST",
                data: { fileName: fname, currentPath: currentPath },
                success: function (res) {                    
                    if (res.status == true) {
                        if (res.message == 'Folder Rename success') {
                            $(".childs, .folder__area").find("[data-val='" + currentPath + "']").find('span, h2').html(res.data.folders[0].name);
                        } else {
                            $(".childs, .folder__area").find("[data-val='" + currentPath + "']").find('span, h2').html(res.data.files[0].name);
                        }
                    } else {
                        bootbox.alert({message: "Error in renaming"})
                    }
                }
            });
        }
    }

    var formClone = `<form action="/" enctype="multipart/form-data" method="POST"><div name="mainFileUploader" class="dropzone" id="my-dropzone"><div class="fallback">
                    <input type="file" class="myFile fl" id=""name="filename[]" multiple></div></div></form><div><button type="submit" id="submit-all"> upload </button></div>`

    //upload btn
    var dropZona;
    $(document).on("click", ".upload__btn", function () {
        box = bootbox.dialog({  //costom bootbox for file upload
            title: '<h2>File Uploads<h2>',
            message: formClone, //html clone
            size: 'large',
            centerVertical : true,
            onEscape: true,
            backdrop: true,
        }).on('shown.bs.modal',function () { //dropzone call
                Dropzone.autoDiscover = false;
                dropZona = new Dropzone("#my-dropzone",{
                url: "php.v2/upload.php",
                autoProcessQueue: false,
                uploadMultiple: true,
                parallelUploads: 100,
                maxFiles: 100,
                addRemoveLinks:true,
                acceptedFiles: '.jpg, .JPG, .PNG, .png, .jpeg, .JPEG, .xls, .XLS, .txt, .TXT, .csv, .CSV, .zip, .ZIP',
                init: function () {
                    this.on('sendingmultiple', function (data, xhr, formData) {  //sending data to php
                        formData.append("currentPath", currentPath);
                    });
                }
            });     
        });           
        $(".bootbox-body").addClass("clearfix")
    });

    //submit btn function
    $(document).on('click', '#submit-all', function () {
        if (!$('.dz-preview').hasClass("dz-error")) { //has error ?
            dropZona.processQueue(); // to start process to upload
            dropZona.on('successmultiple', function (file,res) {
                var obj = JSON.parse(res); //json res data
                if(obj.status == true && obj.message == 'File successfully Upload'){
                    $.each(obj.data.files, function(index,ele){
                        renderItem(ele,currentPath) //append all data
                    })
                }else{
                    bootbox.alert({message: obj.message}); //error message
                }
                box.modal("hide"); 
            })
        }
    })
});