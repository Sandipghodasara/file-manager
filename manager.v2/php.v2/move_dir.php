<?php

$movePath = $_POST["path"]; //move path

$rootPath = '../root/';
$currentPath = $_POST['currentPath'] != "" ? $rootPath.$_POST['currentPath'] : $rootPath; // Actual path 
$allNames = array();
$ext;

if ($_POST["action"] == "move") { // action == move 
    for ($i=0; $i <count($movePath) ; $i++) { 
        global $allNames;
        $actualMovePath = $rootPath.$movePath[$i];
        $tempPath = $movePath[$i];
        $exploded = explode('/', $movePath[$i]);
        $name = end($exploded);
        array_push($allNames,$name);
        $ext = (pathinfo("$tempPath",PATHINFO_EXTENSION));
        rename($actualMovePath, $currentPath."/".end($exploded));
    }

    $msg = "move successfully";
}
 
function nameExt($src) {  //file or folder rename
    $actual_name = pathinfo($src,PATHINFO_FILENAME);
    $extension = pathinfo($src, PATHINFO_EXTENSION);
    $finalPath = "";

    $strsub = strripos($src,"/");
    $update = substr($src,0,$strsub);

    if (!empty($extension)) {
        $i = 1;
        while (file_exists($update."/".$actual_name.".".$extension)) {
            $actual_name = (string)$actual_name.($i);
            $finalPath = $actual_name.'.'.$extension;
            $i++;
        }
       
    }else{
        $i = 1;
        while (file_exists($update."/".$actual_name)) {
            $actual_name = (string)$actual_name.($i);
            $finalPath = $actual_name;
            $i++;
        }
    }

    $finalPath = $update."/".$finalPath;
    return $finalPath;

}

if ($_POST["action"] == "copy") {  //action == copy
    function xcopy($src, $dest) {
        global $allNames;
        if (is_dir($src)) {  //recursive copy function
            if (!file_exists($dest)) {
                mkdir($dest);
            }else{
                $name = nameExt($src);
                $dest = $name;
                mkdir($dest);
                $exploded = explode('/', $dest);
                $new_name = end($exploded);
                array_push($allNames,$new_name);    
            }
            
            foreach (scandir($src) as $file) {
                if ($file === '.' || $file === '..') {
                    continue;
                }
                if (is_dir($src ."/". $file)) {
                    xcopy($src . '/' . $file, $dest . '/' . $file);
                } else {
                    copy($src ."/". $file, $dest . '/' . $file);
                }
            }
            return true;
        }else{  //if only file has than copy direct file 
            $name = nameExt($src);
            $dest = $name;
            copy($src,$dest);
            $exploded = explode('/', $dest);
            $new_name = end($exploded);
            array_push($allNames,$new_name);  
        }

    }
    try {
        for ($i=0; $i <count($movePath) ; $i++) {
            $fileName = pathinfo($movePath[$i], PATHINFO_BASENAME);
            $exploded = explode('/', $movePath[$i]);
            xcopy($rootPath."/".$movePath[$i], $currentPath ."/". end($exploded));
            $ext = (pathinfo($movePath[$i],PATHINFO_EXTENSION));
        }
        $msg = "Copy Successfully";
        

    } catch (\Throwable $th) {
        $msg = "Copy failed";
    }
}

//data
$data = [ 
    "folders" => [], 
    "files"=> [] , 
    'isRoot' => empty($_POST['path']),
    'currentPath' => $_POST['currentPath'], 
    'action' => $_POST["action"],
];

// print_r($allNames);
for($i=0;$i<count($movePath);$i++){

    $currentItme = [
        'name' =>  $allNames[$i],
        'path' => $_POST['currentPath'],
        'type' => $ext != "" ? 'file' : 'folder',
        'extension' => null,
    ];

    if($ext != ""){
        $currentItme['extension'] = $ext;
        $data['files'][]= $currentItme;
    }else{
        $data['folders'][] = $currentItme;
    }
}

//final data
$res = [
    'status' => true,
    'data' => $data,
    'message' => $msg,
    
];

header("Content-Type: json");
echo json_encode($res);

?>