<?php
//erro on page
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$dir = $_POST["path"];
$rootPath = '../root/';
$currentPath = $_POST['currentPath'] != "" ? $rootPath.$_POST['currentPath'] : $rootPath;  // current path 


function recurseRmdir($dir) {  //recusrse delete function
    $files = array_diff(scandir($dir), array('.','..'));
    foreach ($files as $file) {
      (is_dir("$dir/$file")) ? recurseRmdir("$dir/$file") : unlink("$dir/$file");
    }
    return rmdir($dir);
}

for ($i=0; $i < count($dir) ; $i++) {
    $deleteResult = is_dir($rootPath.$dir[$i]) ? recurseRmdir($rootPath.$dir[$i]) : unlink($rootPath.$dir[$i]);
}

//data
$data = [ "folders" => [], "files"=> [] , 'isRoot' => empty($_POST['path']), 'currentPath' => $currentPath ];


//final resonse
$res = [
    'status' => true,
    'data' => $data,
    'message' => $deleteResult == true ?  'Successfully Deleted' : 'Error While Deleting',
];

header("Content-Type: json");
echo json_encode($res);


?>