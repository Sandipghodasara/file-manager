<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$rootPath = '../root/';
$dir = $_POST['currentPath'] != "" ? $rootPath.$_POST['currentPath'] : $rootPath; //actual path
$target_dir = $dir;
$file = $_FILES['file']["name"];

$fileFormat = ['jpg', 'jpeg', 'png', 'xls', 'txt', 'zip', 'csv']; // fileformat 
$msg = '';

for ($i=0; $i < count($file) ; $i++) { 
    if(fileRegx($file[$i]) == 1 || fileRegx($file[$i]) == 2){
        fileRegx($file[$i]) == 1 ? $msg  = 'Enter specific file(zip | png | jpg | JPEG | txt | xls)' : $msg = 'File already Exist';
        break;
    }
}

//data
$data = [ "folders" => [], "files"=> [] , 'isRoot' => empty($_POST['currentPath']), 'currentPath' => $_POST['currentPath'] ];

if(!$msg){ //fi msg is not set
    for($i=0 ; $i < count($file) ; $i++){     
        $target_file = $target_dir ."/". basename($file[$i]);
        move_uploaded_file($_FILES["file"]["tmp_name"][$i] , $target_file); //upload to target dir
        $currentItem = [
            'name' => basename($file[$i]),
            'path' => ($_POST['currentPath'] != "" ? $_POST['currentPath']."/" : "").$file[$i],
            'type' => 'file',
            'extension' => strtolower(pathinfo($target_file,PATHINFO_EXTENSION))
        ];

        $data['files'][] = $currentItem;
    }
    $msg = "File successfully Upload";
}


function fileRegx($file){  //file that check file format and file is exist ?
    global $target_dir, $fileFormat, $uploadErr;
    $uploadErr = 0;
    $target_file = $target_dir ."/".$file;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

    if (!in_array($imageFileType, $fileFormat)){
        $uploadErr = 1;
    }else{
        if (file_exists($target_dir . basename($file))){
            $uploadErr = 2;
        }else{
            $uploadErr = 0;
        }
    }
    return $uploadErr;
}

//respons
$res['status'] = true;
$res['data'] = $data;
$res['message'] = $msg;


header("Content-Type: json");
echo json_encode($res);

?>