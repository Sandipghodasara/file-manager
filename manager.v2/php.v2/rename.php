<?php

$rootPath = '../root/';
$dir = $_POST['currentPath'] != "" ? $rootPath.$_POST['currentPath'] : $rootPath; //actual path
$rename = $_POST["fileName"];

if(is_dir($dir)){
    if(!preg_match("/^[a-zA-Z0-9]*$/", $rename)){  //regex for folder 
        $message = "Folder Rename fail";
    }else{
        $message = reNamea($dir, $rename ,"Folder Rename success");
    }
}else{  //regex for file 
    if(!preg_match("/^.*\.(jpg|JPG|txt|TXT|png|PNG|jpeg|JPEG|xls|XLS|csv|CSV|zip|ZIP)$/", $rename)){
        $message = "File Rename fail";
    }else{
        $message = reNamea($dir,$rename, "File Rename success");
    }
}

function reNamea($path, $rename, $message){  //reanme function
    $last = strripos($path,"/");
    $update = substr($path,0,$last).'/'.$rename;
    rename($path, $update);
    return $message;
}

//data
$data = [ "folders" => [], "files"=> [] , 'isRoot' => empty($_POST['currentPath']), 'currentPath' => $_POST['currentPath'] ];

//current data [file/folder]
$currentItme = [
    'name' =>  $rename,
    'path' => $rename,
    'type' => pathinfo($_POST["fileName"], PATHINFO_EXTENSION) != "" ? 'file' : 'folder',
    'extension' => null,
];

if(pathinfo($_POST["fileName"], PATHINFO_EXTENSION) != ""){
    $currentItme['extension'] = pathinfo($_POST["fileName"], PATHINFO_EXTENSION);
    $data['files'][]= $currentItme;
}else{
    $data['folders'][] = $currentItme;
}

//final response
$res = [
    'status' => true,
    'data' => $data,
    'message' => $message,
    
];

header("Content-Type: json");
echo json_encode($res);

?>