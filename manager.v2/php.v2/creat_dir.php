<?php

$rootPath = '../root/';
$dir = $_POST['path'] != "" ? $rootPath.$_POST['path'] : $rootPath;  //check path is root or not

if ($_POST["fileType"] == "folder") { //req == folder 
    $path = $dir."/".$_POST["fileName"];

    if (!is_dir($path)) {
        mkdir($path, true);
        chmod("$path", 0755);
        $msg = "Folder created success";
    }else{
        $msg = "Folder Already Exist";
    }
}

if ($_POST["fileType"] == "file") { //req == file 
    $filepath = $dir."/".$_POST["fileName"];
    
    if (!file_exists($filepath)) {
        fopen($filepath,"w");
        chmod("$filepath", 0755);
        $msg = "File created success";
    }
    else{
        $msg = "File Already Exist";
    }
}

//data return
$data = [ "folders" => [], "files"=> [] , 'isRoot' => empty($_POST['path']), 'currentPath' => $_POST['path'] ];


if ($_POST["fileType"] == "folder") { // folder than push data in add folder array
    
    $currentItem = [
        'name' => $_POST["fileName"],
        'path' => $_POST['path']."/".$_POST["fileName"],
        'type' => 'folder',
        'extension' => null 
    ];

    $data['folders'][] = $currentItem;

}

if ($_POST["fileType"] == "file") { // file than push data in add file array

    $currentItem = [
        'name' => $_POST["fileName"],
        'path' => $_POST['path']."/".$_POST["fileName"],
        'type' => 'file',
        'extension' => pathinfo($_POST["fileName"], PATHINFO_EXTENSION) 
    ];

    $data['files'][] = $currentItem;
}

$res = [  //final response
    'status' => true,
    'data' => $data,
    'message' => $msg
];

header("Content-Type: json");
echo json_encode($res);


?>