<?php
//erro show
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$rootPath = '../root/';
$dir = $_POST['path'] != "" ? $rootPath.$_POST['path'] : $rootPath; //actual path

$scan_path = scandir($dir);
$scan_path = array_diff($scan_path, ['.','..']);


function getPathAndExt($eleName, $dir){ //getpath and ext
    $newPath["name"] = $eleName;
    $newPath["flag"] = $dir ? true : false;
    $newPath["path"] = ($_POST['path'] != "" ? $_POST['path']."/" : "").$eleName;
    $newPath["ext"] = (pathinfo("$eleName",PATHINFO_EXTENSION));
    return $newPath;
}

$result = [];
foreach( $scan_path as $item) {
    $result[] = getPathAndExt($item,$dir);
}

//data
$data = [ "folders" => [], "files"=> [] , 'isRoot' => empty($_POST['path']), 'currentPath' => $_POST['path'] ];

$data['result'] = $result;

for($i=0;$i<count($scan_path);$i++){
    $currentItme = [
        'name' => $result[$i]["name"],
        'path' => $result[$i]['path'],
        'flag' => $result[$i]['flag'],
        'type' => $result[$i]['ext'] != "" ? 'file' : 'folder',
        'extension' => null,
    ];

    if($result[$i]['ext'] != ""){
        $currentItme['extension'] = $result[$i]['ext'];
        $data['files'][]= $currentItme;
    }else{
        $data['folders'][] = $currentItme;
    }
}


//final resonse
$res = [
    'status' => true,
    'data' => $data,
    'message' => 'success'
];

header("Content-Type: json");
echo json_encode($res);

?>