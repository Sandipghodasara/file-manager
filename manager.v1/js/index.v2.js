$(document).ready(function () {

    var currentPath;
    getContents();

    function getContents(path = null , partial = false, location = null ) {
        $.ajax({
            url: "php.v2/get_dir.php",
            dataType: "json",
            method: "POST",
            data: { path: path },
            success: function (res) {
                if (res.status == true) {
                    renderWorkSpace(res.data, partial, location)
                } else {
                    // bootbox error
                }
            }
        });
    };

    function renderWorkSpace(data, partial , location) {
        currentPath = data.currentPath  
        
        if (data.folders.length > 0) {  
            if (location == "right") {
                $(".folder__area .folderTemplate").siblings().remove();
            } 
            if (location == "left" && partial == false) {
                $(".menu__box .folderTemplate").siblings().remove();
            }  
            data.folders.forEach((element, i) => {
                renderItem(element, currentPath)
            });
        }

        if (data.files.length > 0) {
            data.files.forEach((element, i) => {
                renderItem(element, currentPath)
            });
        }
        
    }

    $(document).on("click", ".create__btn", function () {
        var regx = new RegExp("^[a-zA-Z0-9]*$");
        bootbox.prompt({
            size: "small",
            title: "Enter Folder name...",
            callback: function (result) {
                if (regx.test(result)) {
                   if (result != null) {
                    ajaxCreatCall(result)
                   }else{

                   }
                } else {
                    // bootbox.alert("Enter valid name")
                }
            }
        });

        function ajaxCreatCall(name) {
            $.ajax({
                url: "php.v2/creat_dir.php",
                dataType: "json",
                method: "POST",
                data: { fileName: name, path: currentPath, fileType: "folder" },
                success: function (res) {
                    if (res.status == true) {
                        renderWorkSpace(res.data , true)
                    } else {

                    }
                }
            });
        }
    });

    $(document).on("click", ".leftTree", function () {
       
        currentPath =  $(this).attr("data-val")
        var ext = $(this).find("span").html()

        if (!ext.includes(".")) {
            if ($(this).hasClass("active")) {
                $(this).removeClass("active")
                $(this).find("img").attr("src", "./assets/images/folder.png")
                // _this.siblings(".sub__main").remove()
            } else {
                $(".folder__area .folderTemplate").siblings().remove();
                getContents(currentPath)
                $(this).addClass("active")
                $(this).find("img").attr("src", "./assets/images/openfolder.png")
            }
        }
    });

    $(document).on("click", ".folderClick", function (event) {
        if (!event.ctrlKey) {
            $(this).addClass("folder__select").siblings().removeClass("folder__select")
        } else {
            $(this).addClass("folder__select")
        }
    });

    $(document).on("dblclick", ".folder__area > div", function () {
        var ext = $(this).find("h2").html()
        var clickLocation = $(this).parents().find(".folder__area")
        if (!ext.includes(".")) {
            currentPath = $(this).parent().attr("data-val");
            
            currentPath = $(this).attr("data-val");
            getContents(currentPath, true, "right")
        }
    });

    $(document).on("click", ".back__btn", function () {
        if (currentPath != "../root/") {
            var path = currentPath.split("/");
            var finalPath = path.splice(0, path.length - 2).join("/");

            $(".folderTemplate").siblings().remove();
            getContents(finalPath+"/")
        } else {
            // bootbox.alert("Your can't go <b>BACK</b>")
        }
    });

    $(document).on("click", ".delete__btn", function () {
        var select = $(".folder__select");
        var path = [];

        for (let i = 0; i < select.length; i++) {
            path[i] = select.eq(i).attr("data-val");
        }

        // select.forEach((element) => {
        //     path = this.attr("data-val")
        // });

        bootbox.confirm({
            size: "small",
            message: "<h2>Are you sure want to delet?<h2>",
            callback: function (result) {
                if (result) {
                    ajaxDelCall(path)
                }
            }
        })

        function ajaxDelCall(path) {
            $.ajax({
                url: "php.v2/delete_dir.php",
                dataType: "json",
                method: "POST",
                data: { path : path },
                success: function (res) {
                    $(".folderTemplate").siblings().remove();
                    
                    getContents(currentPath)
                }
            });
        }
    });

    // $(document).on("click", ".add__btn", function () {
        
    //     var regx = new RegExp("^.*\.(jpg|JPG|PNG|png|jpeg|JPEG|xls|XLS|txt|TXT)$");

    //     bootbox.prompt({
    //         size: "small",
    //         title: "Enter File name...",
    //         callback: function (result) {
    //             if (regx.test(result)) {
    //                 ajaxCreatCall(result);
    //             } else {
    //                 // bootbox.alert("Enter valid name")
    //             }
    //         }
    //     });

    //         function ajaxCreatCall(name) {
    //             $.ajax({
    //                 url: "php.v2/creat_dir.php",
    //                 dataType: "json",
    //                 method: "POST",
    //                 data: { fileName: name, path: currentPath, fileType: "file" },
    //                 success: function (res) {
    //                     console.log(res);
    //                     if (res.status == true) {
    //                         renderWorkSpace(res.data)
    //                     } else {
    
    //                     }
    //                 }
    //             });
    //         }
    // });

    function renderItem(data, currentPath, location) {
        // if (data.type == "f") {
        //     var rightSide == {
        //         var $currentItem = $clone.
        //      }
        //     var leftSide == {
        //         var cu
        //         append eft
        //     }
        // }else{

        // }

        var leftFolder = $(".menu__box .folderTemplate").clone()
        leftFolder.find("span").html(data['name']);
        leftFolder.children().attr("data-val", data['path']);
        $(".menu__box").append(leftFolder.html());

        var rightFolder = $(".folder__area .folderTemplate").clone()
        rightFolder.find("h2").html(data['name']);
        rightFolder.children().attr("data-val", data['path']);
        $(".folder__area").append(rightFolder.html());

    }

    // click getCOntents(path)
});
