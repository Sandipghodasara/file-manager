$(document).ready(function () {

    var ele = "../root/";
    var movePath = [], click;
    var _this;


    $(".paste__btn").prop("disabled", true)
    $(".paste__btn").addClass("disable")

    setTimeout(function name() {
        $(".context-menu-icon-paste").addClass("disable")
    }, 200)

    var clone = $(".menu__box").eq(0).html();
    clone = clone.replace("hidden", "");

    var folderHtml = `<div class="file w20 fl">
                            <div>
                                <img src="./assets/images/folder.png" alt="folder" class="w45" >
                                <h2 class="px10 ">Root Folder</h2>
                            </div>
                        </div>`


    init(ele);

    function init(ele) {
        $.ajax({
            url: "php/get_dir.php",
            dataType: "json",
            method: "POST",
            data: { foldername: ele },
            success: function (ele) {
                for (let i = 0; i < ele[0].length; i++) {
                    $(".folder__area").append(folderHtml);

                    $(".file:eq(" + (i) + ")").find("h2").html(ele[1][i]);
                    $(".file:eq(" + (i) + ")").attr("data-val", ele[0][i]);

                    setImg($(".file:eq(" + (i) + ")"), (ele[2][i]))
                }
            }
        });
    }

    function setImg(ele, res) {
        switch (res) {
            case "txt":
                $(ele).children().find("img").attr("src", "./assets/images/txt.png")
                break;
            case "png":
                $(ele).children().find("img").attr("src", "./assets/images/png.png")
                break;
            case "jpg":
                $(ele).children().find("img").attr("src", "./assets/images/jpg.png")
                break;
            case "jpeg":
                $(ele).children().find("img").attr("src", "./assets/images/jpeg.png")
                break;
            case "xls":
                $(ele).children().find("img").attr("src", "./assets/images/xls.png")
                break;
            default:
                $(ele).children().find("img").attr("src", "./assets/images/folder.png")
        }
    }

    function renderLeftSide(val) {
        console.log(val);

        $.ajax({
            url: "php/get_dir.php",
            dataType: "json",
            method: "POST",
            data: { foldername: val },
            success: function (ele) {

                for (let i = 0; i < ele[0].length; i++) {
                    $(".menu__box").append(clone);
                    $(".acc__main").eq(i + 1).find("span").html(ele[1][i]);
                    $(".acc__main").eq(i + 1).attr("data-val", ele[0][i]);

                    setImg($(".acc__main:eq(" + (i + 1) + ")"), (ele[2][i]))
                }
            }
        });
    }

    renderLeftSide(ele);


    function sabLvlRender(val) {

        $.ajax({
            url: "php/get_dir.php",
            dataType: "json",
            method: "POST",
            data: { foldername: val },
            success: function (ele) {

                for (let i = 0; i < ele[0].length; i++) {
                    _this.parent().append(clone);
                    _this.siblings(".acc__main").addClass("sub__main acc__active m10").removeClass("acc__main");
                    _this.siblings((".sub__main:eq(" + (i) + ")")).find("span").html(ele[1][i]);
                    _this.siblings(".sub__main:eq(" + (i) + ")").attr("data-val", ele[0][i]);
                    setImg(_this.siblings((".sub__main:eq(" + (i) + ")")), ele[2][i])
                }
            }
        });
    }



    $(document).on("click", ".main__folder", function () {
        console.log($(this));
        // var _this = $(this);   
        _this = $(this);

        var ext = _this.find("span").html()

        if (!ext.includes(".")) {
            if (_this.parent().hasClass("active")) {
                _this.parent().removeClass("active")
                _this.find("img").attr("src", "./assets/images/folder.png")
                _this.siblings(".sub__main").remove()
            } else {
                $.ajax({
                    url: "php/get_dir.php",
                    method: "POST",
                    data: { foldername: _this.parent().attr("data-val") },
                    dataType: "json",
                    success: function (ele) {
                        for (let i = 0; i < ele[0].length; i++) {
                            _this.parent().append(clone);
                            _this.siblings(".acc__main").addClass("sub__main acc__active m10").removeClass("acc__main");
                            _this.siblings((".sub__main:eq(" + (i) + ")")).find("span").html(ele[1][i]);
                            _this.siblings(".sub__main:eq(" + (i) + ")").attr("data-val", ele[0][i]);
                            setImg(_this.siblings((".sub__main:eq(" + (i) + ")")), ele[2][i])
                        }
                    }
                });
                _this.parent().addClass("active")
                _this.find("img").attr("src", "./assets/images/openfolder.png")
            }
            openRightMenu(_this.parent().attr("data-val"))
        }
    });


    function openRightMenu(val) {
        $(".folder__area").attr("data-val", val)
        $(".folder__area").children().remove()

        $.ajax({
            url: "php/get_dir.php",
            method: "POST",
            data: { foldername: $(".folder__area").attr("data-val") },
            dataType: "json",
            success: function (ele) {
                for (let i = 0; i < ele[0].length; i++) {
                    $(".folder__area").append(folderHtml);
                    $(".folder__area").children((".file:eq(" + (i) + ")")).find("h2").html(ele[1][i]);
                    $(".folder__area").children((".file:eq(" + (i) + ")")).attr("data-val", ele[0][i]);
                    setImg($(".folder__area").children((".file:eq(" + (i) + ")")), ele[2][i])
                }
            }
        });
    }

    $(document).on("dblclick", ".file div", function () {

        var ext = $(this).find("h2").html()

        if (!ext.includes(".")) {
            var path = $(this).parent().attr("data-val")
            // $(".hidden").attr("data-val", path)
            $(".folder__area").attr("data-val", path)
            $(".folder__area").children().remove()

            $.ajax({
                url: "php/get_dir.php",
                method: "POST",
                data: { foldername: $(".folder__area").attr("data-val") },
                dataType: "json",
                success: function (ele) {
                    for (let i = 0; i < ele[0].length; i++) {
                        $(".folder__area").append(folderHtml);
                        $(".folder__area").children((".file:eq(" + (i) + ")")).find("h2").html(ele[1][i]);
                        $(".folder__area").children((".file:eq(" + (i) + ")")).attr("data-val", ele[0][i]);
                        setImg($(".folder__area").children((".file:eq(" + (i) + ")")), ele[2][i])
                    }
                }
            });
        }


    });

    $(document).on("click", ".file div", function (event) {
        if (!event.ctrlKey) {
            $(this).parent().eq($(this).index()).addClass("folder__select").siblings().removeClass("folder__select")
        } else {
            $(this).parent().eq($(this).index()).addClass("folder__select")
        }
    });

    $(document).on("click", ".back__btn", function () {
        if ($(".folder__area").attr("data-val") !== " " && $(".folder__area").attr("data-val") !== "../root/") {
            var path = $(".folder__area").attr("data-val")
            var split1 = path.split("/")

            split1 = split1.splice(0, split1.length - 2)
            var final = split1.join("/");

            $(".folder__area").children().remove()
            $(".folder__area").attr("data-val", (final + "/"))

            $.ajax({
                url: "php/get_dir.php",
                dataType: "json",
                method: "POST",
                data: { foldername: $(".folder__area").attr("data-val") },
                success: function (res) {
                    for (let i = 0; i < res[0].length; i++) {
                        $(".folder__area").append(folderHtml);
                        $(".file:eq(" + (i) + ")").find("h2").html(res[1][i]);
                        $(".file:eq(" + (i) + ")").attr("data-val", res[0][i]);
                        setImg($(".file:eq(" + (i) + ")"), (res[2][i]))
                    }
                }
            });
        } else {
            bootbox.alert("Your can't go <b>BACK</b>")
        }
    });

    $(document).on("click", ".create__btn", function () {
        var regx = new RegExp("^[a-zA-Z0-9]*$");
        bootbox.prompt({
            size: "small",
            title: "Enter Folder name...",
            callback: function (result) {
                if (regx.test(result)) {
                    ajaxCall(result)
                } else {
                    bootbox.alert("Enter valid name")
                }
            }
        });

        function ajaxCall(boxResult) {
            $.ajax({
                url: "php/creat_dir.php",
                dataType: "json",
                method: "POST",
                data: { fileName: boxResult, dir: $(".folder__area").attr("data-val"), fileType: "folder" },
                success: function (res) {
                    console.log('--------temp---------', res);

                    if (res == "Success") {
                        $(".folder__area").children().remove()
                        init($(".folder__area").attr("data-val"))
                        bootbox.alert("success")
                        if (!$(".acc__main").hasClass("active")) {
                            $(".acc__main ").eq(0).siblings().remove();
                            renderLeftSide(ele)
                        } else {
                            _this.siblings().remove();
                            sabLvlRender(_this.parent().attr("data-val"))
                        }
                    } else {
                        bootbox.alert("Already exist")
                    }
                }
            });
        }
    });

    $(document).on("click", ".delete__btn", function () {
        var select = $(".folder__select");
        var dir = [];
        for (let i = 0; i < select.length; i++) {
            dir[i] = select.eq(i).attr("data-val");
        }

        bootbox.confirm({
            size: "small",
            message: "<h2>Are you sure want to delet?<h2>",
            callback: function (result) {
                if (result) {
                    ajaxDelCall(dir)
                }
            }
        })

        function ajaxDelCall(boxResult) {
            $.ajax({
                url: "php/delete_dir.php",
                dataType: "json",
                method: "POST",
                data: { foldername: boxResult },
                success: function (res) {
                    if (res == "Success") {
                        $(".folder__area").children().remove()
                        init($(".folder__area").attr("data-val"))
                        bootbox.alert("success")
                    } else {
                        bootbox.alert("Un-success")
                    }
                }
            });
        }
    });

    $(document).on("click", ".add__btn", function () {
        var currentPath = $(".folder__area").attr("data-val");
        console.log('--------temp---------', currentPath);

        var regx = new RegExp("^.*\.(jpg|JPG|PNG|png|jpeg|JPEG|xls|XLS|txt|TXT)$");


        bootbox.prompt({
            size: "small",
            title: "Enter File name...",
            callback: function (result) {
                if (regx.test(result)) {
                    ajaxCreatCall(result);
                } else {
                    bootbox.alert("Enter valid name")
                }
            }
        });

        function ajaxCreatCall(boxResult) {
            $.ajax({
                url: "php/creat_dir.php",
                dataType: "json",
                method: "POST",
                data: { file: boxResult, dir: currentPath, fileType: "file" },
                success: function (res) {
                    console.log('--------temp---------', res);
                    if (res == "Success") {
                        $(".folder__area").children().remove()
                        init($(".folder__area").attr("data-val"))
                        bootbox.alert("success")
                    } else {
                        bootbox.alert("Already exist")
                    }
                }
            });
        }
    });

    $(document).on("click", ".paste__btn", function () {
        var newPath = $(".folder__area").attr("data-val");
        $.ajax({
            url: "php/move_dir.php",
            dataType: "json",
            method: "POST",
            data: { movedir: movePath, newdir: newPath, action: click },
            success: function (res) {
                console.log('--------temp---------', res);

                if (res == "moved" || res == "Copied") {
                    $(".folder__area").children().remove()
                    init($(".folder__area").attr("data-val"))
                    bootbox.alert("Move successfully")
                } else {
                    bootbox.alert("Already exist")
                }
            }
        });
        $(".paste__btn").prop("disabled", true)
        $(".paste__btn").addClass("disable")
    });

    $(document).on("click", ".move__btn", function () {
        var select = $(".folder__select");
        var dir;
        for (let i = 0; i < select.length; i++) {
            dir = select.eq(i).attr("data-val");
            dir = dir.substring(0, dir.length - 1);
            movePath[i] = dir;
        }
        click = "move";

        $(".paste__btn").prop("disabled", false)
        $(".paste__btn, .context-menu-icon-paste").removeClass("disable")
    });

    $(document).on("click", ".copy__btn", function () {

        var select = $(".folder__select");
        var dir;
        for (let i = 0; i < select.length; i++) {
            dir = select.eq(i).attr("data-val");
            dir = dir.substring(0, dir.length - 1);
            movePath[i] = dir;
        }
        click = "copy";

        $(".paste__btn").prop("disabled", false)
        $(".paste__btn, .context-menu-icon-paste").removeClass("disable")
    });

    $.contextMenu({
        selector: '.file div',
        callback: function(key, options) {
            console.log('--------temp---------', key);

            $(".context-menu-active").parent().addClass("folder__select");
            switch (key) {
                case ("copy"):
                    $(".copy__btn").trigger("click")
                    break;
                case ("cut"):
                    $(".move__btn").trigger("click")
                    break;
                case ("paste"):
                    $(".paste__btn").trigger("click")
                    break;
                case ("delete"):
                    $(".delete__btn").trigger("click")
                    break;
                case ("rename"):
                   rename();
                    break;
                default:
                    break;
            }
        },

        items: {
            "copy": {name: "Copy", icon: "copy"},
            "cut": {name: "Cut", icon: "cut"},
            "delete": {name: "Delete", icon: "delete"},
            "rename": {name: "Rename", icon: "Rename"},
        }
    });

    $.contextMenu({
        selector: '.wrapper:not(.file div)', 
        callback: function(key, options) {
            // $(".context-menu-active").parent().addClass("folder__select");
            switch (key) {
                case ("paste"):
                    $(".paste__btn").trigger("click")
                    break;
                case ("Creat Folder"):
                    $(".create__btn").trigger("click")
                    break;
                case ("Creat File"):
                    $(".add__btn").trigger("click")
                    break;
                default:
                    break;
            }
        },
        items: {
            "paste": {name: "Paste", icon: "paste"},
            "Creat Folder": {name: "Creat Folder", icon: "Folder"},
            "Creat File": {name: "Creat File", icon: "File"},
        }
    });

    function rename() {
        var newPath = $(".folder__select").attr("data-val");

        bootbox.prompt({
            size: "small",
            title: "Rename name...",
            callback: function (result) {
                console.log('--------temp---------', result);
                renameAjax(newPath, result);
            }
        });
        console.log('--------temp---------', newPath);

        function renameAjax(dir, fname) {
            $.ajax({
                url: "php/rename.php",
                dataType: "json",
                method: "POST",
                data: { filename: fname, dir: dir },
                success: function (res) {
                    console.log('--------temp---------', res);
                    if (res[0] == "success") {
                        console.log('--------temp---------', res);

                        $(".folder__area").children().remove()
                        init($(".folder__area").attr("data-val"))
                        bootbox.alert("Rename successfully")
                    } else {
                        bootbox.alert("Reanme Fail")
                    }
                }
            });
        }

    }


// ///////////////////////////////////// comment ////////////////////////////////////////

// $(document).on("click", ".delete__btn", function () {
//     var select = $(".folder__select");
//     var path = [];
//     for (let i = 0; i < select.length; i++) {
//         path[i] = select.eq(i).attr("data-val");
//     }

//     bootbox.confirm({
//         size: "small",
//         message: "<h2>Are you sure want to delet?<h2>",
//         callback: function (result) {
//             if (result) {
//                 ajaxDelCall(path)
//             }
//         }
//     })

//     function ajaxDelCall(paths) {
//         sendAjax('POST', "php.v2/delete_dir.php", paths, getContents)
//         // $.ajax({
//         //     url: "php.v2/delete_dir.php",
//         //     dataType: "json",
//         //     method: "POST",
//         //     data: { path: paths },
//         //     success: function (res) {
//         //         if (res == "Success") {
//         //             if (res.currentPath != "../root/") {
//         //                 $(".menu__box, .folder__area").children().remove();
//         //                 getContents()
//         //                 // bootbox.alert("success")
//         //             } else {

//         //             }
//         //         } else {
//         //             // bootbox.alert("Un-success")
//         //         }
//         //     }
//         // });
//     }
// });

// function sendAjax(type, url, data, callback) {
//     $.ajax({
//         url: "php.v2/delete_dir.php",
//         dataType: "json",
//         method: type,
//         data: { path: data },
//         success: function (res) {
//             if (res == "Success") {
//                 if (callback typeof 'function') {
//                     callback();
//                 }
//                 // if (res.currentPath != "../root/") {
//                 //     $(".menu__box, .folder__area").children().remove();
//                 //     getContents()
//                 //     // bootbox.alert("success")
//                 // } else {

//                 // }
//             } else {
//                 // bootbox.alert("Un-success")
//             }
//         }
//     });
//     }







});