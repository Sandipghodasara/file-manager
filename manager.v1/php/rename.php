<?php

$dir = $_POST["dir"]; 
$rename = $_POST["filename"];

if(is_dir($dir)){
    if(!preg_match("/^[a-zA-Z0-9]*$/", $rename)){
        $message = "fail";
    }else{
        $path = substr($dir,0,-1);
        $last = strripos($path,"/");
        $update = substr($path,0,$last).'/'.$rename;
        rename($path,$update);
        $message = "success";
    }
}else{
    if(!preg_match("/^.*\.(jpg|JPG|txt|TXT|png|PNG|jpeg|JPEG|xls|XLS)$/", $rename)){
        $message = "fail";
    }else{
        $path = substr($dir,0,-1);
        $last = strripos($path,"/");
        $update = substr($path,0,$last).'/'.$rename;
        rename($path,$update);
        $message = "success";
    }
}



print_r(json_encode(array($message,$update)));

?>