<?php

$movedir = $_POST["movedir"]; 
$newdir = $_POST["newdir"];

if ($_POST["action"] == "move") {
    for ($i=0; $i <count($movedir) ; $i++) { 
        $fileName = pathinfo($movedir[$i], PATHINFO_BASENAME);
        rename($movedir[$i], $newdir .$fileName);
    }
    $msg = "moved";
}

function nameExt($src) {
    $actual_name = pathinfo($src,PATHINFO_FILENAME);
    $extension = pathinfo($src, PATHINFO_EXTENSION);
    $finalPath = "";

    $strsub = strripos($src,"/");
    $update = substr($src,0,$strsub);

    if (!empty($extension)) {
        $i = 1;
        while (file_exists($update."/".$actual_name.".".$extension)) {
            $actual_name = (string)$actual_name.($i);
            $finalPath = $actual_name.'.'.$extension;
            $i++;
        }
       
    }else{
        $i = 1;
        while (file_exists($update."/".$actual_name)) {
            $actual_name = (string)$actual_name.($i);
            $finalPath = $actual_name;
            $i++;
        }
    }

 
    $finalPath = $update."/".$finalPath;
    return $finalPath;

}

if ($_POST["action"] == "copy") {
    function xcopy($src, $dest) {

        if (is_dir($src)) {
            if (!file_exists($dest)) {
                mkdir($dest);
            }else{
                $name = nameExt($src);
                $dest = $name;
                mkdir($dest);
            }

            foreach (scandir($src) as $file) {
                if ($file === '.' || $file === '..') {
                    continue;
                }
                if (is_dir($src ."/". $file)) {
                    xcopy($src . '/' . $file, $dest . '/' . $file);
                } else {
                    copy($src ."/". $file, $dest . '/' . $file);
                }
            }
            return true;
        }else{
            $name = nameExt($src);
            $dest = $name;
            copy($src,$dest);
        }

    }
    try {
        for ($i=0; $i <count($movedir) ; $i++) { 
            $fileName = pathinfo($movedir[$i], PATHINFO_BASENAME);
            xcopy($movedir[$i], $newdir . $fileName);
        }
        $msg = "Copied";

    } catch (\Throwable $th) {
        $msg = "Error";
    }
}

print_r(json_encode($msg));

?>